#!/bin/bash

# https://wizardzines.com/comics/bash-errors/bash-errors.png
#set -euo pipefail

exec >> >(tee -i /tmp/install.log)
exec 2>&1

clear

# Friendly introduction.
echo "Welcome to the Arch Linux installation script!"
echo "This script will ERASE ALL DATA on the partition you will choose next!"
echo
read -p "Do you want to continue? Type [Y]es or [N]o. " -n 1 -r
echo
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    exit 1
fi
echo

# Selecting disk.
echo "1. PARTITIONING"
echo "Please select a disk to partition:"
echo

# Don't separate values by space.
IFS=$'\n'

# Set variable containing name and size of all disks.
dsks=( $(lsblk -d | tail -n+2 | awk '{print $1" "$4}') )

# Declare the array.
declare -a dsks

# Select value on array.
select dev in ${dsks[*]}
do

break
done
echo

# Separate values by spaces.
IFS=' '

# Create new variable of selection.
array=($dev)
dev="${array[0]}"

echo "2. ENCRYPTION"
echo

# Setting encryption password.
echo "Choose a strong password for encrypting the primary partition:"
while [ -z "$pwcr" ]; do
echo "Please enter a password: "
read -s pwfr
read -s -p "Retype a password: " pwsc
if [ $pwfr == $pwsc ];
then
  pwcr=$pwfr
  echo
  echo "Both passwords are the same. Continuing.."
  break
else
  echo
  echo "You have entered different passwords. Try again.."
  echo
fi
done
echo

echo "3. HOSTNAME"
echo

# Setting hostname.
echo "Before installing the system, please enter a hostname:"
read host
echo

echo "4. ROOT PASSWORD"
echo

# Setting root password.
echo "Choose a password for the root account:"
while [ -z "$pwr" ]; do
echo "Please enter a password: "
read -s pwfr
read -s -p "Retype a password: " pwsc
if [ $pwfr == $pwsc ];
then
  pwrt=$pwfr
  echo
  echo "Both passwords are the same. Continuing.."
  break
else
  echo
  echo "You have entered different passwords. Try again.."
  echo
fi
done
echo

echo "4. USER"
echo

# Setting username
echo "Please enter a username:"
read user
echo

# Setting user password.
echo "Also please choose a password for your user:"
while [ -z "$pwu" ]; do
echo "Please enter a password: "
read -s pwfr
read -s -p "Retype a password: " pwsc
if [ $pwfr == $pwsc ];
then
  pwur=$pwfr
  echo
  echo "Both passwords are the same. Continuing.."
  break
else
  echo
  echo "You have entered different passwords. Try again.."
  echo
fi
done
echo

echo "5. INSTALLING SYSTEM.."
echo

# Starting partitioning.
echo "Partitioning /dev/$dev.."
echo

# Clearing partition table of selected disk.
echo "Clearing existing partitioning table."
sgdisk -Z /dev/$dev &&
echo

# Creating boot partition.
echo "Creating boot partition of 256 MB."
sgdisk -n 1:0:+256M /dev/$dev &&
echo

# Setting type for EFI.
echo "Setting partition type."
sgdisk -t 1:ef00 /dev/$dev &&
echo

# Creating system partition.
echo "Creating system partition."
sgdisk -n 2:0:0 /dev/$dev
echo

# Print partitions.
echo "This is your new partition table:"
lsblk | grep "$dev"
echo

# Get new variable.
main=${dev}2

# Encrypting partition.
echo "Encrypting system partition. This might take a while."
echo
echo -en "$pwcr\n$pwcr" | cryptsetup -c aes-xts-plain -s 512 luksFormat /dev/$main &&
echo "Partition successfully encrypted."
echo

# Opening encrypted partition and mounting at /dev/mapper/main.
echo "Decrypting.. This also might take a while."
echo
echo -en "$pwcr\n$pwcr" | cryptsetup luksOpen /dev/$main main &&
echo "Partition successfully opened."
echo

lsblk | grep "$dev"
echo

echo "Creating the filesystem."
boot=${dev}1
mkfs.fat -F 32 -n UEFI /dev/$boot &&
echo "Filesystem for boot successfully created."
echo

# Creating btrfs partition.
mkfs.btrfs /dev/mapper/main &&

mount /dev/mapper/main /mnt &&

btrfs subvolume create /mnt/root &&
btrfs subvolume create /mnt/home &&

umount /mnt &&
echo

echo "Mounting.." &&
mount -o autodefrag,compress=zstd:3,subvol=root /dev/mapper/main /mnt &&
mkdir /mnt/home && 
mount -o autodefrag,compress=zstd:3,subvol=home /dev/mapper/main /mnt/home &&
mkdir /mnt/boot &&
mount /dev/$boot /mnt/boot &&
echo "Mounting complete." &&
echo

lsblk
echo

# Updating mirrors using reflector.
echo "Updating mirrors.."
reflector --verbose --latest 10 --country Germany --age 24 --protocol https --sort rate --save /etc/pacman.d/mirrorlist &&
echo "Done."
echo

echo "Updating keyring.."
pacman -Sy archlinux-keyring --noconfirm &&
echo "Done."
echo

# Installing base system.
echo "Installing basic packages.."
pacstrap /mnt base btrfs-progs firefox konsole linux linux-firmware linux-zen nano networkmanager plasma-desktop sddm sddm-kcm sudo &&
echo "Base system installed."
echo

# Generating fstab.
echo "Generating fstab.."
genfstab -Up /mnt > /mnt/etc/fstab &&
echo "Done."
echo

# Setting hostname.
echo "Setting hostname.."
echo $host > /mnt/etc/hostname &&
echo "Done."
echo

# Setting locale.
echo "Setting and generating locale.."
cat << EOF >/mnt/etc/locale.gen
en_US.UTF-8 UTF-8
EOF

#echo "${localegen}" >>/mnt/etc/locale.gen
arch-chroot /mnt locale-gen
echo "LANG=en_US.UTF-8" > /mnt/etc/locale.conf &&
echo "KEYMAP=de-latin1" > /mnt/etc/vconsole.conf &&
echo "Done."
echo

# Set X locale and keyboard alyout.
echo "Setting locale and keyboard layout."

#arch-chroot /mnt localectl --no-convert set-keymap de-latin1-nodeadkeys &&
systemd-firstboot --root=/mnt --keymap=de-latin1-nodeadkeys &&

cat << EOF > /mnt/etc/X11/xorg.conf.d/00-keyboard.conf &&
# Written by systemd-localed(8), read by systemd-localed and Xorg. It's
# probably wise not to edit this file manually. Use localectl(1) to
# instruct systemd-localed to update it.
Section "InputClass"
        Identifier "system-keyboard"
        MatchIsKeyboard "on"
        Option "XkbLayout" "de"
        Option "XkbModel" "pc105"
        Option "XkbVariant" "deadgraveacute"
EndSection
EOF
echo "Done."
echo

# Editing mkinitcpio.conf.
echo "Editing /etc/mkinitcpio.conf.."
#sed -i '7s/.*/MODULES=(ext4)/' /mnt/etc/mkinitcpio.conf &&
sed -i '52s/.*/HOOKS=(base udev autodetect modconf block keyboard keymap encrypt filesystems fsck shutdown)/' /mnt/etc/mkinitcpio.conf &&
echo "Done."
echo

# Creating inital ramdisk.
echo "Creating inital ramdisk for linux-zen.."
arch-chroot /mnt mkinitcpio -p linux-zen &&
echo "Done."
echo

# Enable network and display manager.
echo "Enabling Networkmanager and display manager (sddm).."
arch-chroot /mnt systemctl enable NetworkManager sddm &&
echo "Done."
echo

# Set root password
echo "Setting root password.."
echo -en "$pwrt\n$pwrt" | arch-chroot /mnt passwd
echo "Done."
echo

# Install bootloader.
echo "Installing systemd-boot.."
arch-chroot /mnt bootctl --path=/boot install &&
echo "Done."
echo

# Get UUID for primary partition
uuid=$(ls -l /dev/disk/by-uuid | grep $main | awk '{print $9}')

# Set bootloader entry.
echo "Setting up bootloader.."
cat << EOF > /mnt/boot/loader/entries/arch.conf &&
title   Arch Linux
linux   /vmlinuz-linux-zen
#initrd  /amd-ucode.img 
#initrd  /intel-ucode.img
initrd  /initramfs-linux-zen.img
options cryptdevice=UUID=$uuid:main
options root=/dev/mapper/main rw
options rootflags=subvol=root
options lang=de locale=de_DE.UTF-8
options init=/usr/lib/systemd/systemd
EOF

# Set bootloader fallback entry.
cat << EOF > /mnt/boot/loader/entries/arch-fallback.conf &&
title   Arch Linux Fallback
linux   /vmlinuz-linux
initrd  /initramfs-linux-fallback.img
options cryptdevice=UUID=$uuid:main
options root=/dev/mapper/main rw
options rootflags=subvol=root
options lang=de locale=de_DE.UTF-8
options init=/usr/lib/systemd/systemd
EOF

# Set bootloader.
echo "timeout 1" >> /mnt/boot/loader/loader.conf &&
echo "default arch.conf" >> /mnt/boot/loader/loader.conf &&
echo "Done."
echo

# Set username, password and group.
echo "Adding user.."
arch-chroot /mnt useradd -m $user &&
echo -en "$pwur\n$pwur" | arch-chroot /mnt passwd $user &&
arch-chroot /mnt gpasswd -a $user wheel &&
sed -i '82s/.*/%wheel ALL=(ALL) ALL/' /mnt/etc/sudoers
echo "Done."
echo

# Enabling sddm auto-login.
echo "Enabling auto-login.."
mkdir /mnt/etc/sddm.conf.d &&
cat << EOF > /mnt/etc/sddm.conf.d/autologin.conf
[Autologin]
User=$user
Session=plasma
EOF
echo "Done."
echo

# Adding sddm config.
cat << EOF > /mnt/etc/sddm.conf.d/kde_settings.conf
[Autologin]
Relogin=false
Session=plasma
User=$user

[General]
HaltCommand=/usr/bin/systemctl poweroff
RebootCommand=/usr/bin/systemctl reboot

[Theme]
Current=breeze

[Users]
MaximumUid=60513
MinimumUid=1000
EOF

# Unmounting and rebooting.
echo "Unmounting system.."
umount /mnt/boot &&
umount /mnt &&
echo "Done."
echo

clear

echo "Installation finished!"
#read -p "Do you want to read the log? Type [Y]es or [N]o. " -n 1 -r
#echo
#if [[ ! $REPLY =~ ^[Yy]$ ]]
#then
#    less /tmp/install.log
#fi
#echo

#echo "Press [Enter] to shut down, then remove the installation media and start your system."
#poweroff
